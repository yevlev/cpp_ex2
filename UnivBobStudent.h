/**
 * UnivBobStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a student in 'Bob' university,
 *          inherits from class UniversityStudent.
 *
 * Methods:  UnivBobStudent()    - Constructor. 
 *								   Gets student's id number, salary, average grade.
 *			 ~UnivBobStudent()   - Destructor.
 *
 *			 getUnivAvg() - static method that returns university 'Bob' average grade.
 *
 *			 getInstitutionName() - getter for institution name.
 * --------------------------------------------------------------------------------------
 */


#ifndef UNIV_BOB_STUDENT_H
#define UNIV_BOB_STUDENT_H

#include <string>

#include "UniversityStudent.h"

using namespace std;

class UnivBobStudent : public UniversityStudent
{

public:

	UnivBobStudent(const string& idNumber, double salary, double avgGrade);

	virtual ~UnivBobStudent();

	virtual const string& getInstitutionName() const;

	static double getUnivAvg();

private:

	const string _institutionName;
	static int s_numOfStudents;
	static double s_sumOfAvgGrades;
	
};

#endif
