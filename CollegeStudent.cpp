/**
 * The implementation of class CollegeStudent, 
 * which represents a student in a college, and inherits from class Student.
 *
 */

#include "CollegeStudent.h"


/**
* Constructor.
* Gets student's id number, salary, average grade, project grade.
*
*/
CollegeStudent::CollegeStudent(const string& idNumber, double salary,
								double projGrade, double avgGrade) : Student(idNumber, salary),
								_projGrade(projGrade), _avgGrade(avgGrade)
{

}


/**
* Destructor.
*/
CollegeStudent::~CollegeStudent()
{

}


/**
* getter for average grade.
*/
double CollegeStudent::getAvgGrade() const
{
	return this->_avgGrade;
}

/**
* getter for project grade.
*/
double CollegeStudent::getProjGrade() const
{
	return this->_projGrade;
}

/**
* setter for average grade.
*
*/
void CollegeStudent::setAvgGrade(double avgGrade)
{
	this->_avgGrade = avgGrade;
}

/**
* setter for project grade.
*
*/
void CollegeStudent::setProjGrade(double projGrade)
{
	this->_projGrade = projGrade;
}

