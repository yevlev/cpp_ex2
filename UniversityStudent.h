/**
 * UniversityStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a student in a university,
 *          inherits from class Student.
 *
 * Methods:  UniversityStudent()    - Constructor. 
 *									  Gets student's id number, salary and average grade.
 *			 ~UniversityStudent()   - Destructor.
 *
 *			 getAvgGrade() - getter for average grade.
 *           setAvgGrade() - setter for average grade.
 *
 *			 getInstitutionName() - abstract method for institution name.
 * --------------------------------------------------------------------------------------
 */

#ifndef UNIVERSITY_STUDENT_H
#define UNIVERSITY_STUDENT_H

#include <string>

#include "Student.h"

using namespace std;


class UniversityStudent : public Student
{

public:

	UniversityStudent(const string& idNumber, double salary, double avgGrade);

	virtual ~UniversityStudent();

	double getAvgGrade() const;

	void setAvgGrade(double avgGrade);

	virtual const string& getInstitutionName() const = 0;

private:

	double _avgGrade;

	//to forbid copy (even no need to implement them).
	UniversityStudent(const UniversityStudent& other);
	const UniversityStudent& operator=(const UniversityStudent& other);

};

#endif
