/**
 * The implementation of class UnivBobStudent, which represents a student in 'Bob' university,
 * inherits from class UniversityStudent.
 *
 */

#include "UnivBobStudent.h"


/**
* Constructor.
* Gets student's id number, salary, average grade.
*/
UnivBobStudent::UnivBobStudent(const string& idNumber, double salary, double avgGrade) : 
								UniversityStudent(idNumber, salary, avgGrade),
								_institutionName("univBob")
{
	s_numOfStudents++;   //as we created new instance of student in univ. 'Bob'.
	s_sumOfAvgGrades = s_sumOfAvgGrades + this->getAvgGrade();
}

/**
* Destructor.
*
*/
UnivBobStudent::~UnivBobStudent()
{
	s_numOfStudents--;   //as we destroyed an instance of student in univ. 'Bob'.
	s_sumOfAvgGrades = s_sumOfAvgGrades - this->getAvgGrade();
}

/**
* getter for institution name.
*
*/
const string& UnivBobStudent::getInstitutionName() const
{

	return this->_institutionName;

}

/**
* static mathod that returns the average of
* 'Bob' university.
*/
double UnivBobStudent::getUnivAvg()
{
	if (s_numOfStudents == 0)
	{
		return 0;
	}

	return (s_sumOfAvgGrades / s_numOfStudents);
}


/* init. of static data members. */

int UnivBobStudent::s_numOfStudents = 0;
double UnivBobStudent::s_sumOfAvgGrades = 0;
