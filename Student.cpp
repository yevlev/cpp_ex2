/**
 * The implementation of class Student, 
 * which represents a general student.
 *
 */

#include "Student.h"


/**
* Constructor. 
* Gets student's id number and desired salary.
*/
Student::Student(const string& idNumber, double salary) : _idNumber(idNumber), _salary(salary)
{

}


/**
* Destructor.
*
*/
Student::~Student()
{

}

/**
* getter for id number.
*
*/
const string& Student::getIdNumber() const
{
	return this->_idNumber;
}

/**
* getter for salary.
*
*/
double Student::getSalary() const
{
	return this->_salary;
}


/**
* setter for salary.
*
*/
void Student::setSalary(double salary)
{
	this->_salary = salary;
}
