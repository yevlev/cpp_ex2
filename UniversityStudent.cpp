/**
 * The implementation of class UniversityStudent, 
 * which represents a student in a university, inherits from class Student.
 *
 */

#include "UniversityStudent.h"

/**
* Constructor.
* Gets student's id number, salary, average grade.
*/
UniversityStudent::UniversityStudent(const string& idNumber, double salary,
									double avgGrade) : Student(idNumber, salary),
									_avgGrade(avgGrade)
{

}
										

/**
* Destructor.
*
*/
UniversityStudent::~UniversityStudent()
{

}

/**
* getter for average grade.
*
*/
double UniversityStudent::getAvgGrade() const
{
	return this->_avgGrade;
}


/**
* setter for average grade.
*
*/
void UniversityStudent::setAvgGrade(double avgGrade)
{
	this->_avgGrade = avgGrade;
}
