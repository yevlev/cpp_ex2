/**
* the file with the main function. 
*
* here we recieve the input from user - info. about candidate students.
* we determine who are the 2 best qualified students, and output the.
* 
*/

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "Student.h"
#include "CollegeStudent.h"
#include "UniversityStudent.h"
#include "CollegeColinStudent.h"
#include "CollegeDannyStudent.h"
#include "UnivAliceStudent.h"
#include "UnivBobStudent.h"

#define DOUBLE_PREC 4
#define INIT_STUD_ARR_SIZE 16
#define EXPANSION_RATE 2
#define A_B_DIFF 7
#define A_C_DIFF 10
#define A_D_DIFF 15
#define PROJ_WEIGHT 0.6
#define GRADE_WEIGHT 0.4
#define SUM_SALARIES 30000

using namespace std;

/**
* a function that receives the command line array, and parses the cmd line
* arguments (flags), filling inputFile, outputFile, according to given flags.
*/
void getCmdLineArgs(int argc, char** argv, char** inputFile, char** outputFile)
{
	if (argc == 1)
	{
		return;  //no input or output files.
	}

	if (argc == 3)  //single flag given.
	{
		if (strcmp(argv[1], "-i") == 0)
		{
			*inputFile = argv[2];
			return;

		}

		if (strcmp(argv[1], "-o") == 0)
		{
			*outputFile = argv[2];
			return;
		}
	
	}

	/* for sure argc == 5 here */

	if (strcmp(argv[1], "-i") == 0)
	{
		*inputFile = argv[2];
		*outputFile = argv[4];
		return;	
	}

	*inputFile = argv[4];   //vice versa from prev.
	*outputFile = argv[2];
}


/**
* a help function puts in detail, an info. about student,
* getting it from a line from input, from startInd till next space.
*
*/
void getStudDetail(const string& inputLine, string& detail, int* startInd)
{
	int endInd = inputLine.find(" ", *startInd);

	detail = inputLine.substr(*startInd, (endInd - *startInd));

	*startInd = endInd + 1;
}


/**
* a factory method for students. 
* creates the required instances of concrete students according to their institution name,
* from input file.
*
*/
Student* studentFactory(const string& institutionName, const string& idNumber, 
						double avgGrade, double salary, double projGrade = 0)
{
	if (institutionName == "A")
	{
		return (new UnivAliceStudent(idNumber, salary, avgGrade));
	}

	if (institutionName == "B")
	{
		return (new UnivBobStudent(idNumber, salary, avgGrade));
	}

	if (institutionName == "C")
	{
		return (new CollegeColinStudent(idNumber, salary, projGrade, avgGrade));
	}

	if (institutionName == "D")
	{
		return (new CollegeDannyStudent(idNumber, salary, projGrade, avgGrade));
	}

	return nullptr;  //we should never get here.
}

/**
* a function that inserts students to students array, expanding it if needed.
*
*/
void insertToArray(Student** studArray, int* arrayLen, int* lastInd, Student* stud)
{
	if (*lastInd == *arrayLen)  //we need to expand the array.
	{
		Student** tempArray = new Student*[(*arrayLen) * EXPANSION_RATE];
		
		for (int i = 0; i < *arrayLen; i++)
		{
			tempArray[i] = studArray[i];
		}

		delete[] studArray;

		studArray = tempArray;
		*arrayLen = (*arrayLen) * EXPANSION_RATE;
	}

	studArray[*lastInd] = stud;
	(*lastInd)++;
}


/**
* a function that parses the input from user (from file, or standard input),
* filling the students array.
*
*/
int parseInput(istream is, Student** arr, int* arrLen, int* lastInd)
{
	string line, idNumber, salary, institutionName, projGrade, avgGrade;

	int startInd = 0;
	bool isCollegeStud = false;
	
	while (1)
	{
		getline(is, line);

		if (line.empty())  //end of input.
		{
			break;
		}

		getStudDetail(line, institutionName, &startInd);
		getStudDetail(line, idNumber, &startInd);
		getStudDetail(line, avgGrade, &startInd);

		if ((institutionName == "C") || (institutionName == "D"))  //college.
		{
			getStudDetail(line, projGrade, &startInd);
			isCollegeStud = true;
		}

		getStudDetail(line, salary, &startInd);

		Student* stud;
		
		if (isCollegeStud == true)
		{
			stud = studentFactory(institutionName, idNumber, 
									atof(avgGrade.c_str()), atof(salary.c_str()), 
									atof(projGrade.c_str()));
		}
		else
		{
			//we don't have here the proj. garde.
			stud = studentFactory(institutionName, idNumber, 
									atof(avgGrade.c_str()), atof(salary.c_str()));
		}

		insertToArray(arr, arrLen, lastInd, stud);

		isCollegeStud = false;
	}

	return 0;
}


/**
* function which calculates the worthiness of a student according to his 
* average grade and institution, as defined in the ex.
*
*/
double getValue(const Student* stud)
{
	const string& instituteName = stud->getInstitutionName();
	
	if (instituteName == "univAlice")
	{
		return ((UniversityStudent*)stud)->getAvgGrade(); //just the average.
	}

	if (instituteName == "univBob")
	{
		double avgPropAB = UnivAliceStudent::getUnivAvg() / UnivBobStudent::getUnivAvg();
		double ret = ((((UniversityStudent*)stud)->getAvgGrade()) * avgPropAB) + A_B_DIFF;
	
		return ret;
	}

	if (instituteName == "collegeColin")
	{
		double avgPropAC = UnivAliceStudent::getUnivAvg() / CollegeColinStudent::getCollegeAvg();

		double ret = PROJ_WEIGHT * ((CollegeStudent*)stud)->getProjGrade() +
						GRADE_WEIGHT * ((CollegeStudent*)stud)->getAvgGrade();

		ret = (ret * avgPropAC) - A_C_DIFF;
	
		return ret;
	}

	if (instituteName == "collegeDanny")
	{
		double avgPropAD = UnivAliceStudent::getUnivAvg() / CollegeDannyStudent::getCollegeAvg();

		double ret = PROJ_WEIGHT * ((CollegeStudent*)stud)->getProjGrade() +
						GRADE_WEIGHT * ((CollegeStudent*)stud)->getAvgGrade();

		ret = (ret * avgPropAD) - A_D_DIFF;
	
		return ret;
	}

	return 0; /* we shouldn't get here. */
}



/**
* returns student's institution name.
*
*/
string getInstitutionName(const Student* stud)
{
	const string& instituteName = stud->getInstitutionName();
	
	if (instituteName == "univAlice")
	{
		return ("Alice");
	}

	if (instituteName == "univABob")
	{
		return ("Bob");
	}

	if (instituteName == "collegeColin")
	{
		return ("Colin");
	}

	if (instituteName == "collegeDanny")
	{
		return ("Danny");
	}
}


/**
* printing function of student output, in desired format.
*
*/
void printStudent(ostream os, const Student* stud, bool isCollege)
{
	string collegeOrUniv = (isCollege) ? "college" : "univ";

	double avg = (isCollege) ? ((CollegeStudent*)stud)->getAvgGrade() : 
								((UniversityStudent*)stud)->getAvgGrade();

	os << "ID: " << atoi(stud->getIdNumber().c_str()) << " |" << collegeOrUniv << ": " 
		<<  getInstitutionName(stud) << " |average: " << setiosflags(ios::fixed)
		<< setprecision(DOUBLE_PREC) << avg << " |"; 
		
	if (isCollege == true)  //we shall print also the proj. garde.
	{
		os << "project: " <<setiosflags(ios::fixed) << setprecision(DOUBLE_PREC)
			<< ((CollegeStudent*)stud)->getProjGrade() << " |";
	}

	os << "worth: " << setiosflags(ios::fixed) << setprecision(DOUBLE_PREC) << getValue(stud) ;

}


/**
* function that gets students array, and outputs the best 2 students,
* to the desired output stream.
*/
void produceOutput(ostream os, Student** studArr, int lastInd)
{
	int firstBestStudent = 0;
	int secondBestStudent = 1;
	double bestSum = 0;

	//calculate students values.
	for (int i = 0; i < lastInd; i++)
	{
		for (int j = i + 1; j < lastInd; j++)
		{
			if (studArr[i]->getSalary() + studArr[j]->getSalary() <= SUM_SALARIES)
			{
				double sum = getValue(studArr[i]) + getValue(studArr[j]);

				if (sum > bestSum)
				{
					bestSum = sum;
					firstBestStudent = i;
					secondBestStudent = j;
				}
			}
		}
	}
	

	//who has lowest salary - must be first.
	if (studArr[firstBestStudent]->getSalary() > studArr[secondBestStudent]->getSalary())
	{
		int temp = firstBestStudent;
		firstBestStudent = secondBestStudent;
		secondBestStudent = temp;
	}
	
	/* check and save where the 2 students study - univ. or college.
	   using the 'univ' prefix that has university institution name. */
	bool isFirstStudCollege = !(studArr[firstBestStudent]->getInstitutionName().find("univ") == 0);
	bool isSecStudCollege = !(studArr[secondBestStudent]->getInstitutionName().find("univ") == 0);


	//prints both students, sending fitting univ./college flag
	printStudent(os, studArr[firstBestStudent], isFirstStudCollege);
	
	printStudent(os, studArr[firstBestStudent], isSecStudCollege);

}


/**
* the main function. 
* receives command line arguments,
* and calls different methods that read and parse the input,
* and output best 2 students.
*/
int main(int argc, char** argv) 
{
	if ((argc != 1) && (argc != 3) && (argc != 5))
	{
		cerr << "num of params to main illegal - usage: [-i <input_file>]" << 
				" [-o <output_file>]" << endl ;
		return 1;
	}

	char* inputFile = nullptr;
	char* outputFile = nullptr;
	
	getCmdLineArgs(argc, argv, &inputFile, &outputFile);

	//array of students, with init. size.
	Student** studArr = new Student*[INIT_STUD_ARR_SIZE];
	
	int arrLen = INIT_STUD_ARR_SIZE;
	int lastIndex = 0;  //first empty index of array.

	if (inputFile != nullptr)  //we read from input file.
	{
		ifstream ifs(inputFile);
		ifs.open(inputFile);
		parseInput(ifs, studArr, &arrLen, &lastIndex);
		ifs.close();
	}
	else
	{
		parseInput(cin, studArr, &arrLen, &lastIndex);
	}


	if (outputFile != nullptr) //we write to output file.
	{
		ofstream ofs(outputFile);
		ofs.open(outputFile);
		produceOutput(ofs, studArr, lastIndex);
		ofs.close();
	}
	else
	{
		produceOutput(cout, studArr, lastIndex);
	}


	//freeing memory.
	for (int i = 0; i < lastIndex; i++)
	{
		delete studArr[i];
	}

	delete[] studArr;

	return 0;
}
