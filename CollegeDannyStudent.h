/**
 * CollegeDannyStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a student in 'Danny' college,
 *          inherits from class CollegeStudent.
 *
 * Methods:  CollegeDannyStudent()    - Constructor.
 *                                      Gets student's id number, salary, average grade, 
 *                                      project grade.
 *			 ~CollegeDannyStudent()   - Destructor.
 *
 *			 getCollegeAvg() - static method that returns college 'Danny' average grade.
 *
 *			 getInstitutionName() - getter for institution name.
 * --------------------------------------------------------------------------------------
 */


#ifndef COLLEGE_DANNY_STUDENT_H
#define COLLEGE_DANNY_STUDENT_H

#include <string>

#include "CollegeStudent.h"

using namespace std;

class CollegeDannyStudent : public CollegeStudent
{

public:

	CollegeDannyStudent(const string& idNumber, double salary, double projGrade, double avgGrade);

	virtual ~CollegeDannyStudent();

	virtual const string& getInstitutionName() const;

	static double getCollegeAvg();

private:

	const string _institutionName;
	static int s_numOfStudents;
	static double s_sumOfAvgGrades;
	
};

#endif
