/**
 * CollegeColinStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a student in 'Colin' college,
 *          inherits from class CollegeStudent.
 *
 * Methods:  CollegeColinStudent()    - Constructor. 
 *                                      Gets student's id number, salary, average grade, 
 *                                      project grade.
 *			 ~CollegeColinStudent()   - Destructor.
 *
 *			 getCollegeAvg() - static method that returns college 'Colin' average grade.
 *
 *			 getInstitutionName() - getter for institution name.
 * --------------------------------------------------------------------------------------
 */


#ifndef COLLEGE_COLIN_STUDENT_H
#define COLLEGE_COLIN_STUDENT_H

#include <string>

#include "CollegeStudent.h"

using namespace std;

class CollegeColinStudent : public CollegeStudent
{

public:

	CollegeColinStudent(const string& idNumber, double salary, double projGrade, double avgGrade);

	virtual ~CollegeColinStudent();

	virtual const string& getInstitutionName() const;

	static double getCollegeAvg();

private:

	const string _institutionName;
	static int s_numOfStudents;
	static double s_sumOfAvgGrades;
	
};

#endif
