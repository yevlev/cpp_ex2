/**
 * The implementation of class UnivAliceStudent, which represents a student in 'Alice' university,
 * inherits from class UniversityStudent.
 *
 */

#include "UnivAliceStudent.h"

/**
* Constructor.
* Gets student's id number, salary, average grade.
*/
UnivAliceStudent::UnivAliceStudent(const string& idNumber, double salary, double avgGrade) : 
									UniversityStudent(idNumber, salary, avgGrade),
									_institutionName("univAlice")
{
	s_numOfStudents++;   //as we created new instance of student in univ. 'Alice'.
	s_sumOfAvgGrades = s_sumOfAvgGrades + this->getAvgGrade();
}

/**
* Destructor.
*
*/
UnivAliceStudent::~UnivAliceStudent()
{
	s_numOfStudents--;   //as we destroyed an instance of student in univ. 'Alice'.
	s_sumOfAvgGrades = s_sumOfAvgGrades - this->getAvgGrade();
}

/**
* getter for institution name.
*
*/
const string& UnivAliceStudent::getInstitutionName() const
{

	return this->_institutionName;

}


/**
* static mathod that returns the average of
* 'Alice' university.
*
*/
double UnivAliceStudent::getUnivAvg()
{
	if (s_numOfStudents == 0)
	{
		return 0;
	}

	return (s_sumOfAvgGrades / s_numOfStudents);
}


/* init. of static data members. */

int UnivAliceStudent::s_numOfStudents = 0;
double UnivAliceStudent::s_sumOfAvgGrades = 0;
