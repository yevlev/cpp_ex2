/**
 * UnivAliceStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a student in 'Alice' university,
 *          inherits from class UniversityStudent.
 *
 * Methods:  UnivAliceStudent()    - Constructor.
 *									 Gets student's id number, salary, average grade.
 *			 ~UnivAliceStudent()   - Destructor.
 *
 *			 getUnivAvg() - static method that returns university 'Alice' average grade.
 *
 * 			 getInstitutionName() - getter for institution name.
 * --------------------------------------------------------------------------------------
 */


#ifndef UNIV_ALICE_STUDENT_H
#define UNIV_ALICE_STUDENT_H

#include <string>

#include "UniversityStudent.h"

using namespace std;

class UnivAliceStudent : public UniversityStudent
{

public:

	UnivAliceStudent(const string& idNumber, double salary, double avgGrade);

	virtual ~UnivAliceStudent();

	static double getUnivAvg();

	virtual const string& getInstitutionName() const;

private:

	const string _institutionName;
	static int s_numOfStudents;
	static double s_sumOfAvgGrades;
	
};

#endif
