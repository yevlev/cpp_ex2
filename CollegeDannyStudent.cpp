/**
 * The implementation of class CollegeDannyStudent, which represents a student in 'Danny' college,
 * inherits from class CollegeStudent.
 *
 */

#include "CollegeDannyStudent.h"

/**
* Constructor.
* Gets student's id number, salary, average grade, project grade.
*/
CollegeDannyStudent::CollegeDannyStudent(const string& idNumber, double salary, 
										double projGrade, double avgGrade) : 
										CollegeStudent(idNumber, salary,
										projGrade, avgGrade),
										_institutionName("collegeDanny")
{
	s_numOfStudents++;   //as we created new instance of student in college 'Danny'.
	s_sumOfAvgGrades = s_sumOfAvgGrades + this->getAvgGrade();
}

/**
* Destructor.
*/
CollegeDannyStudent::~CollegeDannyStudent()
{
	s_numOfStudents--;   //as we destroyed an instance of student in college 'Danny'.
	s_sumOfAvgGrades = s_sumOfAvgGrades - this->getAvgGrade();
}


/**
* getter for institution name.
*
*/
const string& CollegeDannyStudent::getInstitutionName() const
{

	return this->_institutionName;

}

/**
* static mathod that returns the average of
* 'Danny' college.
*
*/
double CollegeDannyStudent::getCollegeAvg()
{
	if (s_numOfStudents == 0)
	{
		return 0;
	}

	return (s_sumOfAvgGrades / s_numOfStudents);
}


/* init. of static data members. */

int CollegeDannyStudent::s_numOfStudents = 0;
double CollegeDannyStudent::s_sumOfAvgGrades = 0;