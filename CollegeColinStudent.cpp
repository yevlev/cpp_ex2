/**
 * The implementation of class CollegeColinStudent, which represents a student in 'Colin' college,
 * inherits from class CollegeStudent.
 *
 */

#include "CollegeColinStudent.h"

/**
* Constructor.
* Gets student's id number, salary, average grade, project grade.
*/
CollegeColinStudent::CollegeColinStudent(const string& idNumber, double salary,
											double projGrade, double avgGrade) : 
											CollegeStudent(idNumber, salary,
											projGrade, avgGrade),
											_institutionName("collegeColin")
{
	s_numOfStudents++;   //as we created new instance of student in college 'Colin'.
	s_sumOfAvgGrades = s_sumOfAvgGrades + this->getAvgGrade();
}

/**
* Destructor.
*/
CollegeColinStudent::~CollegeColinStudent()
{
	s_numOfStudents--;   //as we destroyed an instance of student in college 'Colin'.
	s_sumOfAvgGrades = s_sumOfAvgGrades - this->getAvgGrade();
}


/**
* getter for institution name.
*
*/
const string& CollegeColinStudent::getInstitutionName() const
{

	return this->_institutionName;

}


/**
* static mathod that returns the average of
* 'Colin' college.
*
*/
double CollegeColinStudent::getCollegeAvg()
{	
	if (s_numOfStudents == 0)
	{
		return 0;
	}

	return (s_sumOfAvgGrades / s_numOfStudents);
}


/* init. of static data members. */

int CollegeColinStudent::s_numOfStudents = 0;
double CollegeColinStudent::s_sumOfAvgGrades = 0;