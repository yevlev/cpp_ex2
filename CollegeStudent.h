/**
 * CollegeStudent.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a student in a college,
 *          inherits from class Student.
 *
 *
 * Methods:  CollegeStudent()    - Constructor.
 *								   Gets student's id number, salary, average grade, project grade.
 *			 ~CollegeStudent()   - Destructor.
 *
 *			 getAvgGrade() - getter for average grade.
 *			 getProjGrade() - getter for project garde.
 *
 *			 setAvgGrade() - setter for average grade.
 *			 setProjGrade() - setter for project garde.
 *
 *  		 getInstitutionName() - abstract method for institution name.
 * --------------------------------------------------------------------------------------
 */

#ifndef COLLEGE_STUDENT_H
#define COLLEGE_STUDENT_H

#include <string>

#include "Student.h"

using namespace std;


class CollegeStudent : public Student
{

public:

	CollegeStudent(const string& idNumber, double salary, double projGrade, double avgGrade);

	virtual ~CollegeStudent();

	double getAvgGrade() const;
	double getProjGrade() const;

	void setAvgGrade(double avgGrade);
	void setProjGrade(double projGrade);

	virtual const string& getInstitutionName() const = 0;

private:
	
	double _projGrade;
	double _avgGrade;

	//to forbid copy (even no need to implement them).
	CollegeStudent(const CollegeStudent& other);
	const CollegeStudent& operator=(const CollegeStudent& other);

};

#endif
